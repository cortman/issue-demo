# Gitlab Issue Demo / Presentation
## Basic defect tracking
First and foremost, issues are a means for tracking software defects and change requests.

[Please put something helpful in the readme! (#1) · Issues · Ortman, Chris E / issue-demo · GitLab](https://git.uiowa.edu/cortman/issue-demo/issues/1)

What’s nice about having them sit along side your code is the way you can link to files directly and show inline diffs & changes.
It also allows you to skip needing to go to a separate UI to _close_ the issue after you finish working on it because the issues can react to things you put in your commit messages.

_Show commit message push and close issue_

Issues also support a neat thing called quick actions you can for example 

```
/estimate 5 days
```

To set a time estimate on your issue.

## Organizing & Managing using boards
You can use labels in order to organize issues into a board view.

Issues can also be viewed using an issue board. I really like boards because I think they do a good job at conveying meaningful information at a glance.

![](Gitlab%20Issue%20Demo%20:%20Presentation/4A52F9DC-0A16-4C6E-AFF9-C6AD12696060.png)

vs

![](Gitlab%20Issue%20Demo%20:%20Presentation/0C553DDB-D763-4771-84B2-FF5922A293B7.png)

You set up labels for each lane and as you drag the issue between lanes the old label is removed and the new label added.

This can help make bottlenecks obvious and visible to everyone. And helps answer big questions at a glance. 

If we adopt a consistent / shared labeling concepts then we can have this visibility to the whole group.

## More uses for issues

But they don’t have to be limited to software defects.
Any time you want to have some content that requires revising over time (git) and a discussion around those changes issues are a great tool

### Examples:

Tracking language proposals in documents and using issues for change / discussion

[GitHub - apple/swift-evolution: This maintains proposals for changes and user-visible enhancements to the Swift Programming Language.](https://github.com/apple/swift-evolution)

[GitHub - emberjs/rfcs: RFCs for changes to Ember](https://github.com/emberjs/rfcs)

Using a repository with example configuration and allowing the customers to collaborate with the developers via issues in order to refine the syntax

[GitHub - microsoft/rfc_customguestconfig: Customer Collaborated Design for Azure Policy Guest Configuration custom content](https://github.com/microsoft/rfc_customguestconfig)

Managing a meetup group talk / presentation proposals. Open an issue == propose a talk

[Issues · sfdevs/meetups · GitHub](https://github.com/sfdevs/meetups/issues)

> You can use this just about any place you would instead use a word doc that is emailed around with track changes turned on.  

When you combine these things you can build some nice tools

team project admin & roadmap





